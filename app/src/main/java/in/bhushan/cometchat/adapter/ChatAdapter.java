package in.bhushan.cometchat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.pojo.Message;

/**
 * Created by Bhushan Chaudhari
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatHolder> {

    List<Message> msgList;


    public ChatAdapter(List<Message> msgList) {
        this.msgList = msgList;
    }

    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0: {
                return new ChatHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_left, parent, false));
            }
            case 1: {
                return new ChatHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_right, parent, false));
            }
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        Message msg = msgList.get(position);
        return (msg.getSelf() == 1) ? 1 : 0;
    }


    @Override
    public void onBindViewHolder(@NonNull ChatHolder holder, int position) {
        Message m = msgList.get(position);

        //Log.d("ChatAdapter", "<--IN ADAPTER-->" + m);

        holder.sender_label.setText(m.getMsgFrom());
        holder.msg.setText(m.getMessage());
        holder.timestamp.setText(new Date(m.getSent() * 1000L).toString());
    }

    @Override
    public int getItemCount() {
        return msgList.size();
    }


    class ChatHolder extends RecyclerView.ViewHolder {
        private TextView sender_label, msg, timestamp;

        public ChatHolder(View itemView) {
            super(itemView);

            sender_label = itemView.findViewById(R.id.sender_text_view);
            msg = itemView.findViewById(R.id.message_text_view);
            timestamp = itemView.findViewById(R.id.timestamp_text_view);
        }
    }
}
