package in.bhushan.cometchat.adapter;

/**
 * Created by Bhushan Chaudhari
 */

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.db.AppDatabase;
import in.bhushan.cometchat.pojo.GroupMessage;
import in.bhushan.cometchat.pojo.Message;


public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.GroupChatHolder> {

    private List<GroupMessage> msgList;
    private String id;



    public GroupChatAdapter(List<GroupMessage> msgList, String id) {
        this.msgList = msgList;
        this.id = id;
    }

    @NonNull
    @Override
    public GroupChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0: {
                return new GroupChatHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_left, parent, false));
            }
            case 1: {
                return new GroupChatHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_right, parent, false));
            }
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        GroupMessage msg = msgList.get(position);
        return (msg.getFromid().equals(id)) ? 1 : 0;
    }


    @Override
    public void onBindViewHolder(@NonNull GroupChatHolder holder, int position) {
        GroupMessage m = msgList.get(position);

        //Log.d("ChatAdapter", "<--IN ADAPTER-->" + m);

        holder.sender_label.setText(m.getFrom());
        holder.msg.setText(m.getMessage());
        holder.timestamp.setText(new Date(Long.parseLong(m.getSent()) * 1000L).toString());
    }

    @Override
    public int getItemCount() {
        return msgList.size();
    }


    class GroupChatHolder extends RecyclerView.ViewHolder {
        private TextView sender_label, msg, timestamp;

        public GroupChatHolder(View itemView) {
            super(itemView);

            sender_label = itemView.findViewById(R.id.sender_text_view);
            msg = itemView.findViewById(R.id.message_text_view);
            timestamp = itemView.findViewById(R.id.timestamp_text_view);
        }
    }
}

