package in.bhushan.cometchat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.bhushan.cometchat.R;
import in.bhushan.cometchat.listener.AdapterListener;
import in.bhushan.cometchat.pojo.ChatContact;

/**
 * Created by Bhushan Chaudhari
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ChatContactViewHolder> {


    private List<ChatContact> chatContactList;
    private AdapterListener<ChatContact> adapterListener;

    public ContactAdapter(List<ChatContact> chatContactList, AdapterListener<ChatContact> adapterListener) {
        this.chatContactList = chatContactList;
        this.adapterListener = adapterListener;
    }

    @NonNull
    @Override
    public ChatContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = null;
        if (viewType == 0) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_contact_item, parent, false);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_contact_item, parent, false);
        }
        return new ChatContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatContactViewHolder holder, int position) {
        holder.name.setText(chatContactList.get(position).getName());

        holder.linearLayout.setOnClickListener((View v) -> {
            adapterListener.getClickedUser(chatContactList.get(position));
        });

        holder.circleImageView.setOnClickListener((View v) -> {
            adapterListener.getClickOnImage(chatContactList.get(position));
        });
    }

    @Override
    public int getItemViewType(int position) {
        ChatContact chatContact = chatContactList.get(position);
        return chatContact.getIsGroup();
    }

    @Override
    public int getItemCount() {
        return chatContactList.size();
    }

    class ChatContactViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        LinearLayout linearLayout;
        CircleImageView circleImageView;

        public ChatContactViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nameTitle);
            linearLayout = itemView.findViewById(R.id.ll_2);
            circleImageView = itemView.findViewById(R.id.profile_image);
        }
    }

}
