package in.bhushan.cometchat.listener;

/**
 * Created by Bhushan Chaudhari
 */
public interface AdapterListener<T> {
    void getClickedUser(T t);
    void getClickOnImage(T t);
}

