package in.bhushan.cometchat.listener;

/**
 * Created by Bhushan Chaudhari
 */
public interface InternalCallback<T> {
    void onResponse(T t);
}
