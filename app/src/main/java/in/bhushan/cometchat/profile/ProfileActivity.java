package in.bhushan.cometchat.profile;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.common.CommonHelper;
import in.bhushan.cometchat.pojo.Profile;
import in.bhushan.cometchat.pojo.User;

/**
 * Created by Bhushan Chaudhari
 */

public class ProfileActivity extends AppCompatActivity implements ProfileView {

    private String uid;
    private TextView name,status,status_message;
private ProfilePresenter profilePresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        uid = getIntent().getStringExtra("id");
        profilePresenter = new ProfilePresenterImpl(this);
        init();

        CommonHelper.showLoadingProgressDialog(this);
        profilePresenter.getProfile(uid);
    }

    private void init(){
        name = findViewById(R.id.name);
        status = findViewById(R.id.status);
        status_message = findViewById(R.id.status_message);

    }

    @Override
    public void getProfile(User profile) {

        CommonHelper.removeLoadingProgressDialog();
        if(profile!=null){
            name.setText(profile.getName());
            status.setText(profile.getStatus());
            status_message.setText(profile.getStatusMessage());
        }
    }
}
