package in.bhushan.cometchat.profile;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.inscripts.interfaces.Callbacks;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.db.AppDatabase;
import in.bhushan.cometchat.pojo.Profile;
import in.bhushan.cometchat.pojo.User;

/**
 * Created by Bhushan Chaudhari
 */
public class ProfilePresenterImpl implements ProfilePresenter {


    private ProfileView profileView;
    private AppDatabase mDb;
    private Context mContext;
    private CometChat cometChat;


    public ProfilePresenterImpl(ProfileView profileView) {
        this.profileView = profileView;
        mDb = App.getDB();
        mContext = App.getContext();
        cometChat = App.getCometChatInstance();
    }


    @Override
    public void getProfile(String uid) {

        Gson gson = new Gson();
        cometChat.getUserInfo(uid, new Callbacks(){
            @Override
            public void successCallback(JSONObject response){ /* Code Block */
                Log.d("ProfilePresenterImpl", "response:" + response);

                Gson gson = new Gson();
                 //profileView.getProfile(gson.fromJson(response.toString(),Profile.class));
                 profileView.getProfile(gson.fromJson(response.toString(),User.class));
            }

            @Override
            public void failCallback(JSONObject response){ /* Code Block */

                Log.d("ProfilePresenterImpl", "response:" + response);
                profileView.getProfile(null);
            }
        });

    }
}
