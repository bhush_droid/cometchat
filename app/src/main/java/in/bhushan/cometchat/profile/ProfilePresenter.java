package in.bhushan.cometchat.profile;

/**
 * Created by Bhushan Chaudhari
 */
public interface ProfilePresenter {


    void getProfile(String uid);
}
