package in.bhushan.cometchat.profile;

import in.bhushan.cometchat.pojo.Profile;
import in.bhushan.cometchat.pojo.User;

/**
 * Created by Bhushan Chaudhari
 */
public interface ProfileView {

    void getProfile(User profile);

}
