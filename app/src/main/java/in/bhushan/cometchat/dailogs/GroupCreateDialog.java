package in.bhushan.cometchat.dailogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonChatPresenterImpl;
import in.bhushan.cometchat.common.CommonHelper;
import in.bhushan.cometchat.common.Constant;
import in.bhushan.cometchat.listener.InternalCallback;
import in.bhushan.cometchat.pojo.InternalResponse;

/**
 * Created by Bhushan Chaudhari
 */
public class GroupCreateDialog extends Dialog implements GroupCreateView {

    private CommonChatPresenter commonChatPresenter;
    private EditText groupName_ET;
    private Button btn1;

    public GroupCreateDialog(@NonNull Context context) {
        super(context);
    }

    public GroupCreateDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected GroupCreateDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_create_group);
        commonChatPresenter = new CommonChatPresenterImpl();
        groupName_ET = findViewById(R.id.group_name);
        btn1 = findViewById(R.id.ok_btn);


        btn1.setOnClickListener(v ->{
            validateEditText();
        });
    }

    @Override
    public void createGroup(String groupName) {
        commonChatPresenter.createGroup(groupName, internalResponse -> {
            if (internalResponse.isStatus()) {
                commonChatPresenter.getGroupList();
                groupName_ET.setText("");
                Toast.makeText(App.getContext(), groupName + "Group Created", Toast.LENGTH_SHORT).show();
                dismiss();
            }else{

                commonChatPresenter.getGroupList();
                groupName_ET.setText("");
                CommonHelper.showAlertDialog(App.getContext(),"ERROR","ERROR Creating Group", Constant.ALERT_ERROR);
                dismiss();
            }
        });
    }

    @Override
    public void validateEditText() {
        if (groupName_ET.getText() == null || groupName_ET.getText().toString() == "") {
            groupName_ET.setError("Group Name cannot be null");
        }else{
            createGroup(groupName_ET.getText().toString());
        }
    }


}
