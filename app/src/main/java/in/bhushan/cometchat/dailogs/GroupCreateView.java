package in.bhushan.cometchat.dailogs;

/**
 * Created by Bhushan Chaudhari on 7/4/2018 12:49 PM.
 */
public interface GroupCreateView {

    void createGroup(String groupName);
    void validateEditText();
}
