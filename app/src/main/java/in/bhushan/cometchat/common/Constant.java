package in.bhushan.cometchat.common;

/**
 * Created by Bhushan Chaudhari
 */
public class Constant {

    /**
     *@Desc Alert Types
     */
    public static final byte ALERT_INFO = 0;
    public static final byte ALERT_WARNING = 1;
    public static final byte ALERT_ERROR = 2;
    public static final byte ALERT_SUCCESS = 3;

}
