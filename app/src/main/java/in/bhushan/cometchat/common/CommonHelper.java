package in.bhushan.cometchat.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import in.bhushan.cometchat.R;

/**
 * Created by Bhushan Chaudhari
 */
public class CommonHelper {

    private static ProgressDialog mProgressDialog;

    public static void showLoadingProgressDialog(Context context) {
        showSimpleProgressDialog(context, "", "Loading ...", false);
    }

    public static void removeLoadingProgressDialog() {
        removeSimpleProgressDialog();
    }

    public static void showLoadingProgressDialog(Context context,String message) {
        showSimpleProgressDialog(context, "", message, false);
    }



    public static void showSimpleProgressDialog(Context context, String title,
                                                String msg, boolean isCancelable) {

        try {
            mProgressDialog = null;
            mProgressDialog = ProgressDialog.show(context, title, msg);
            TextView tv1 =  mProgressDialog.findViewById(android.R.id.message);
            tv1.setTextColor(context.getResources().getColor(R.color.textColor));
            mProgressDialog.setCancelable(isCancelable);

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public static void removeSimpleProgressDialog() {
        try {
            mProgressDialog.dismiss();
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public static void showAlertDialog(Context context, String title, String message, byte status) {
        showAlertDialog(context,title,message,status,null);
    }

    public static void showAlertDialog(Context context, String title, String message, byte status, final DialogInterface.OnClickListener listener) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        TextView tv1 =  mProgressDialog.findViewById(android.R.id.message);
        tv1.setTextColor(context.getResources().getColor(R.color.textColor));
        alertDialog.setMessage(message);
        // Setting alert dialog icon
        int icon = R.drawable.vd_info;
        if (status == Constant.ALERT_INFO) icon = R.drawable.vd_info;
        if (status == Constant.ALERT_WARNING) icon = R.drawable.vd_warning;
        if (status == Constant.ALERT_ERROR) icon = R.drawable.vd_error;
        if (status == Constant.ALERT_SUCCESS) icon = R.drawable.vd_success;

        if(listener!=null) {
            alertDialog.setCancelable(false);
        }
        alertDialog.setIcon(icon);
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if(listener!=null){
                    listener.onClick(alertDialog,1);
                }else {
                    alertDialog.dismiss();
                }
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
