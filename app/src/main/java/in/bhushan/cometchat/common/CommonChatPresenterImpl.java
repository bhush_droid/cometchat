package in.bhushan.cometchat.common;

import android.app.ActivityManager;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.inscripts.enums.GroupType;
import com.inscripts.interfaces.Callbacks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.db.AppDatabase;
import in.bhushan.cometchat.listener.InternalCallback;
import in.bhushan.cometchat.pojo.GroupMessage;
import in.bhushan.cometchat.pojo.Groups;
import in.bhushan.cometchat.pojo.InternalResponse;
import in.bhushan.cometchat.pojo.Message;
import in.bhushan.cometchat.pojo.Profile;
import in.bhushan.cometchat.pojo.User;
import in.bhushan.cometchat.service.ChatService;

/**
 * Created by Bhushan Chaudhari
 */
public class CommonChatPresenterImpl implements CommonChatPresenter {


    private CometChat cometChat;
    private AppDatabase mDb;
    private Context mContext;


    public CommonChatPresenterImpl() {
        this.cometChat = App.getCometChatInstance();
        this.mDb = App.getDB();
        this.mContext = App.getContext();
    }

    @Override
    public void getOnlineList() {
        getOnlineList(null);
    }


    @Override
    public void getOnlineList(InternalCallback<Boolean> callback) {

        cometChat.getOnlineUsers(new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                try {
                    parseUserData(jsonObject);
                    if(callback!=null) {
                      callback.onResponse(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failCallback(JSONObject jsonObject) {

            }
        });


    }

    @Override
    public void getGroupList() {

        cometChat.getAllGroup(new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                try {
                    parseGroupData(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failCallback(JSONObject jsonObject) {

            }
        });

    }


    private Profile getUserInfo() {



       return null;//mDb.profileModel().getProfile();

    }

    @Override
    public void setMessages(JSONObject message) {

        try {
            if (message.has("Messages")) {
                parseMultipleMessages(message.getJSONArray("Messages"), message.getInt("count"));

            } else if(message.has("history")) {
                parseMultipleMessages(message.getJSONArray("history"), 0);
            }else{
                parseSingleMessage(message);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setGroupMessages(JSONObject message) {

        try {
            if (message.has("Messages")) {
                parseMultipleGroupMessages(message.getJSONArray("Messages"), message.getInt("count"));

            } else if(message.has("history")) {
                parseMultipleGroupMessages(message.getJSONArray("history"), 0);
            }else{
                parseSingleGroupMessage(message);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAllUsers() {
        return mDb.userModel().getAllUsers();
    }

    @Override
    public LiveData<List<Groups>> getAllGroups() {
         return mDb.groupModel().getAllGroups();
    }



    @Override
    public void createGroup(String groupName,InternalCallback<InternalResponse> callback) {

        cometChat.createGroup(groupName, "", GroupType.PUBLIC_GROUP, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("CommonChatPresenterImpl", "group Icon" + jsonObject);

                if(jsonObject.has("error")){
                    try {
                        Toast.makeText(App.getContext(), jsonObject.get("error").toString(), Toast.LENGTH_SHORT).show();
                        callback.onResponse(new InternalResponse(jsonObject,false)       );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    callback.onResponse(new InternalResponse(jsonObject,true));
                }


            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                callback.onResponse(new InternalResponse(jsonObject,false));
            }
        });
    }

    @Override
    public void logout() {
        cometChat.logout(new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("CommonChatPresenterImpl", "<-------Logout-------- : " + jsonObject);
                clearDataAndLogout();
            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.d("CommonChatPresenterImpl", "<-------Logout-------- : " + jsonObject);
                clearDataAndLogout();
            }
        });
    }



    private void clearDataAndLogout(){
        stopService();
        mDb.clearAllTables();
        PreferenceHelper.setLoginStatus(App.getContext(),false);
        PreferenceHelper.setUID(App.getContext(),null);
    }


    @Override
    public void getProfileInfo() {
        Log.d("CommonChatPresenterImpl", "<-------------------Profile ------------->" + getUserInfo());
    }


    private void parseSingleMessage(JSONObject message) {
        Gson gson = new Gson();
        Message msg = gson.fromJson(message.toString(), Message.class);
        saveMessage(msg);
    }


    private void parseMultipleMessages(JSONArray msgArray, int count) throws JSONException {
        Gson gson = new Gson();
        for (int i = 0; i < msgArray.length(); i++) {
            saveMessage(gson.fromJson(msgArray.getJSONObject(i).toString(), Message.class));
        }

    }

    private void parseSingleGroupMessage(JSONObject message) {
        Gson gson = new Gson();
        GroupMessage msg = gson.fromJson(message.toString(), GroupMessage.class);
        saveGroupMessage(msg);
    }

    private void parseMultipleGroupMessages(JSONArray msgArray, int count) throws JSONException {
        Gson gson = new Gson();

        for (int i = 0; i < msgArray.length(); i++) {
            saveGroupMessage(gson.fromJson(msgArray.getJSONObject(i).toString(), GroupMessage.class));
        }
    }


    @Override
    public void parseRecentData(JSONObject recentMessages) throws JSONException {
        Gson gson = new Gson();
        Log.d("CommonChatPresenterImpl", recentMessages.toString());
        List<Message> msgList = new ArrayList<>();

        Iterator<String> keysItr = recentMessages.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            msgList.add(gson.fromJson(recentMessages.getJSONObject(key).getJSONObject("m").toString(), Message.class));
        }
        Log.d("Message List", msgList.toString());

        saveMessage(msgList);
    }

    /*
    {"42":{"id":"59","m":{"id":59,"from":46,"message":"hi","self":1,"old":0,"sent":1530511367,"message_type":10,"to":"42"},"t":"1530511367","s":1,"n":"demo 4","a":"\/\/fast.cometondemand.net\/admin\/images\/pixel.png"},
        "51":{"id":"75","m":{"id":75,"from":46,"message":"hello","self":1,"old":0,"sent":1530696102,"message_type":10,"to":"51"},"t":"1530696102","s":1,"n":"121","a":"\/\/fast.cometondemand.net\/admin\/images\/pixel.png"},
        "50":{"id":"78","m":{"id":78,"from":46,"message":"hello 6:03","self":1,"old":0,"sent":1530707605,"message_type":10,"to":"50"},"t":"1530707605","s":1,"n":"CaptainAmerica","a":"\/\/fast.cometondemand.net\/admin\/images\/pixel.png"},
        "45":{"id":"73","m":{"id":73,"from":46,"message":"test","self":1,"old":0,"sent":1530617119,"message_type":10,"to":"45"},"t":"1530617119","s":1,"n":"Spiderman","a":"\/\/fast.cometondemand.net\/images\/spiderman.png"}}
    */


    private void parseUserData(JSONObject users) throws JSONException {
        Gson gson = new Gson();
        Log.d("CommonChatPresenterImpl", users.toString());
        List<User> userList = new ArrayList<>();

        Iterator<String> keysItr = users.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            userList.add(gson.fromJson(users.getJSONObject(key).toString(), User.class));
        }
        Log.d("Users List", userList.toString());

        saveUserData(userList);
    }





    private void saveUserData(List<User> userList) {
        if (userList.size() > 0) {
            for (User user : userList) {
                mDb.userModel().insert(user);
                Log.d("CommonChatPresenterImpl", "User : ----->  " + user);
            }
        } else {
            Log.d("CommonChatPresenterImpl", "user list is empty");
        }
    }


    @Override
    public void parseGroupData(JSONObject group) throws JSONException {
        Gson gson = new Gson();
        Log.d("CommonChatPresenterImpl", group.toString());
        List<Groups> groupList = new ArrayList<>();

        Iterator<String> keysItr = group.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            groupList.add(gson.fromJson(group.getJSONObject(key).toString(), Groups.class));
        }
        Log.d("group List", groupList.toString());
        saveGroupData(groupList);
    }


    private void saveGroupData(List<Groups> userList) {
        if (userList.size() > 0) {
            for (Groups group : userList) {
                mDb.groupModel().insert(group);
                Log.d("CommonChatPresenterImpl", "group : ----->  " + group);
            }
        } else {
            Log.d("CommonChatPresenterImpl", "group list is empty");
        }
    }


    private void saveMessage(Message message) {
        Log.d("CommonChatPresenterImpl", "-------Message-------->" + message);

        mDb.messageModel().insert(message);
    }


    private void saveGroupMessage(GroupMessage message) {
        Log.d("CommonChatPresenterImpl", "-------Message-------->" + message);

        mDb.groupMessageModel().insert(message);
    }


    private void saveMessage(List<Message> msgList) {
        if (msgList.size() > 0) {
            mDb.messageModel().insert(msgList);
        }
    }


    @Override
    public void startService(){
        if(!isMyServiceRunning(ChatService.class)) {
            Intent intent = new Intent(mContext, ChatService.class);
            mContext.startService(intent);
        }
    }


    private void stopService() {
        if(isMyServiceRunning(ChatService.class)) {
            Intent intent = new Intent(mContext, ChatService.class);
            mContext.stopService(intent);
        }
    }


    @Override
    public void restartService(){
        Intent intent = new Intent(mContext, ChatService.class);
        if(!isMyServiceRunning(ChatService.class)) {
            mContext.startService(intent);
        }else{
            mContext.stopService(intent);
            mContext.startService(intent);
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}


