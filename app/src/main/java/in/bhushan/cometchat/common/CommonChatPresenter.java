package in.bhushan.cometchat.common;

import android.arch.lifecycle.LiveData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import in.bhushan.cometchat.listener.InternalCallback;
import in.bhushan.cometchat.pojo.Groups;
import in.bhushan.cometchat.pojo.InternalResponse;
import in.bhushan.cometchat.pojo.User;

/**
 * Created by Bhushan Chaudhari
 */
public interface CommonChatPresenter {

    void getOnlineList();

    void getOnlineList(InternalCallback<Boolean> callback);

    void getGroupList();

    void setMessages(JSONObject message);

    void setGroupMessages(JSONObject message);

    List<User> getAllUsers();

    LiveData<List<Groups>> getAllGroups();

    void createGroup(String groupName, InternalCallback<InternalResponse> callback);

    void logout();

    void getProfileInfo();

    void parseRecentData(JSONObject recentMessages) throws JSONException;

    void parseGroupData(JSONObject group) throws JSONException;

    void startService();

    void restartService();
}
