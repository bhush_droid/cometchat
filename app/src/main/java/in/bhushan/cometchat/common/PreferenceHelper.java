package in.bhushan.cometchat.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Bhushan Chaudhari
 */
public class PreferenceHelper {


    private static final String pref_name = "server_event";
    private static final int mode = Context.MODE_PRIVATE;


    private static final String LOGIN_STATUS = "login_status";
    private static final String UID = "uid";


    /****************************************************************************************/
    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(pref_name, mode);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return getSharedPreferences(context).edit();
    }

    public static boolean contains(Context context, String key) {
        return getSharedPreferences(context).contains(key);
    }

    private static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    private static boolean readBoolean(Context context, String key, boolean defValue) {
        return getSharedPreferences(context).getBoolean(key, defValue);
    }

    private static void writeInt(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();
    }

    private static int readInt(Context context, String key, int defValue) {
        return getSharedPreferences(context).getInt(key, defValue);
    }

    private static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    private static float readFloat(Context context, String key, float defValue) {
        return getSharedPreferences(context).getFloat(key, defValue);
    }

    private static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    private static long readLong(Context context, String key, long defValue) {
        return getSharedPreferences(context).getLong(key, defValue);
    }


    private static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();
    }

    private static String readString(Context context, String key, String defValue) {
        return getSharedPreferences(context).getString(key, defValue);
    }

    /****************************************************************************************/

    public static void setLoginStatus(Context context, boolean status) {
        writeBoolean(context, LOGIN_STATUS, status);
    }

    public static boolean getLoginStatus(Context context){
        return readBoolean(context,LOGIN_STATUS,false);
    }
    public static void setUID(Context context,String uid){
        writeString(context,UID,uid);
    }

    public static String getUID(Context context){
        return  readString(context,UID,null);
    }


}
