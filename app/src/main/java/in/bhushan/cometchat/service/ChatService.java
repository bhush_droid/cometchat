package in.bhushan.cometchat.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.inscripts.interfaces.SubscribeCallbacks;

import org.json.JSONException;
import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonChatPresenterImpl;
import in.bhushan.cometchat.db.AppDatabase;
import in.bhushan.cometchat.pojo.Groups;
import in.bhushan.cometchat.pojo.Profile;

/**
 * Created by Bhushan Chaudhari
 */
public class ChatService extends Service {

    private CometChat cometChat;
    private CommonChatPresenter commonChatPresenter;
    private AppDatabase mDb;


    @Override
    public void onCreate() {
        super.onCreate();
        cometChat = App.getCometChatInstance();
        mDb = App.getDB();
        commonChatPresenter = new CommonChatPresenterImpl();

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("ChatService", "-------------> Service started <------------");

        cometChat.subscribe(true, new SubscribeCallbacks() {
            @Override
            public void gotOnlineList(JSONObject jsonObject) {
                Log.d("ChatService", "gotOnlineList -----------> " + jsonObject);

            }

            @Override
            public void gotBotList(JSONObject jsonObject) {
                Log.d("ChatService", "gotBotList -----------> " + jsonObject);

            }

            @Override
            public void gotRecentChatsList(JSONObject jsonObject) {
                Log.d("ChatService", "gotRecentChatsList -----------> " + jsonObject);
                try {
                    commonChatPresenter.parseRecentData(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(JSONObject jsonObject) {
                Log.d("ChatService", "onError -----------> " + jsonObject);
            }

            @Override
            public void onMessageReceived(JSONObject jsonObject) {
                Log.d("ChatService", "onMessageReceived -----------> " + jsonObject);
                commonChatPresenter.setMessages(jsonObject);
            }

            @Override
            public void gotProfileInfo(JSONObject jsonObject) {
                Log.d("ChatService", "gotProfileInfo -----------> " + jsonObject);

                Gson gson = new Gson();
                Profile p  = gson.fromJson(jsonObject.toString(), Profile.class);
                mDb.profileModel().insert(p);
                Log.d("ChatService", "< ----------- profile is -------- >   " + p);

            }

            @Override
            public void gotAnnouncement(JSONObject jsonObject) {
                Log.d("ChatService", "gotAnnouncement -----------> " + jsonObject);
            }

            @Override
            public void onAVChatMessageReceived(JSONObject jsonObject) {
                Log.d("ChatService", "onAVChatMessageReceived  -----------> " + jsonObject);
            }

            @Override
            public void onActionMessageReceived(JSONObject jsonObject) {
                Log.d("ChatService", "onActionMessageReceived -----------> " + jsonObject);
            }

            @Override
            public void onGroupMessageReceived(JSONObject jsonObject) {
                Log.d("ChatService", "onGroupMessageReceived -----------> " + jsonObject);
                commonChatPresenter.setGroupMessages(jsonObject);

            }

            @Override
            public void onGroupsError(JSONObject jsonObject) {
                Log.d("ChatService", "onGroupsError -----------> " + jsonObject);
            }

            @Override
            public void onLeaveGroup(JSONObject jsonObject) {
                Log.d("ChatService", "onLeaveGroup-----------> " + jsonObject);
            }

            @Override
            public void gotGroupList(JSONObject jsonObject) {
                Log.d("ChatService", "gotGroupList -----------> " + jsonObject);
                try {
                    commonChatPresenter.parseGroupData(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void gotGroupMembers(JSONObject jsonObject) {
                Log.d("ChatService", "gotGroupMembers -----------> " + jsonObject);
            }

            @Override
            public void onGroupAVChatMessageReceived(JSONObject jsonObject) {
                Log.d("ChatService", "onGroupAVChatMessageReceived    -----------> " + jsonObject);
            }

            @Override
            public void onGroupActionMessageReceived(JSONObject jsonObject) {
                Log.d("ChatService", "onGroupActionMessageReceived  -----------> " + jsonObject);
            }
        });


        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
