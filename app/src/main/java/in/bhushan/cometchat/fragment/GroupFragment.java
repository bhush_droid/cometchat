package in.bhushan.cometchat.fragment;


import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import in.bhushan.cometchat.GroupChat.GroupChatActivity;
import in.bhushan.cometchat.R;
import in.bhushan.cometchat.adapter.ContactAdapter;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonChatPresenterImpl;
import in.bhushan.cometchat.listener.AdapterListener;
import in.bhushan.cometchat.pojo.ChatContact;
import in.bhushan.cometchat.pojo.Groups;

/**
 * Created by Bhushan Chaudhari
 */

public class GroupFragment extends Fragment implements AdapterListener<ChatContact> {


    private RecyclerView rv_group;

    private ContactAdapter contactAdapter;
    private CommonChatPresenter commonChatPresenter;


    public GroupFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        commonChatPresenter =  new CommonChatPresenterImpl();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_group, container, false);

        init(rootView);
        getAllGroups();

        return rootView;
    }


    private void init(View rootView) {

        rv_group = rootView.findViewById(R.id.rv_group);

    }


    private void getAllGroups() {
        LiveData<List<Groups>> userListLiveData = commonChatPresenter.getAllGroups();

        userListLiveData.observe(this,userList ->{

            List<ChatContact> chatContactList = new ArrayList<>();
            for (Groups group : userList
                    ) {
                chatContactList.add(new ChatContact(group.getId(), group.getName(), "", 1));
                Log.d("GroupFragment", "group from DB :---------> " + group);
            }
            contactAdapter = new ContactAdapter(chatContactList, this);
            rv_group.setLayoutManager(new LinearLayoutManager(getActivity()));
            rv_group.setAdapter(contactAdapter);


        });
    }

    @Override
    public void getClickedUser(ChatContact chatContact) {

        Log.d("MainActivity", "contact Details ------> " + chatContact);
        Intent intent = new Intent(getActivity(), GroupChatActivity.class).putExtra("id", chatContact.getId());
        startActivity(intent);
    }

    @Override
    public void getClickOnImage(ChatContact chatContact) {

    }
}
