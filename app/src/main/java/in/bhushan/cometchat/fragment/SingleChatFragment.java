package in.bhushan.cometchat.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.adapter.ContactAdapter;
import in.bhushan.cometchat.chat.ChatActivity;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonChatPresenterImpl;
import in.bhushan.cometchat.listener.AdapterListener;
import in.bhushan.cometchat.listener.InternalCallback;
import in.bhushan.cometchat.pojo.ChatContact;
import in.bhushan.cometchat.pojo.User;
import in.bhushan.cometchat.profile.ProfileActivity;

/**
 * Created by Bhushan Chaudhari
 */

public class SingleChatFragment extends Fragment implements AdapterListener<ChatContact> {


    private RecyclerView rv1;
    private ContactAdapter contactAdapter;
    private CommonChatPresenter commonChatPresenter;
    List<ChatContact> contactList;

    public SingleChatFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        commonChatPresenter = new CommonChatPresenterImpl();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_single_chat, container, false);
        init(rootView);
        initrv();

        contactList =  getAllUsers();

        if(contactList.size() > 0){
            initrv();
        }else{
            commonChatPresenter.getOnlineList(new InternalCallback<Boolean>() {
                @Override
                public void onResponse(Boolean aBoolean) {
                    initrv();
                }
            });
        }

        return rootView;
    }


    private void init(View rootView) {
        rv1 = rootView.findViewById(R.id.rv_1);
    }


    @Override
    public void getClickedUser(ChatContact chatContact) {
        Log.d("MainActivity", "contact Details ------> " + chatContact);
        Intent intent = new Intent(getActivity(), ChatActivity.class).putExtra("id", chatContact.getId());
        startActivity(intent);

    }

    @Override
    public void getClickOnImage(ChatContact chatContact) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class).putExtra("id", chatContact.getId());
        startActivity(intent);
    }


    private void initrv() {

        contactAdapter = new ContactAdapter(getAllUsers(), this);
        rv1.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv1.setAdapter(contactAdapter);

    }


    private List<ChatContact> getAllUsers() {
        List<User> userList = commonChatPresenter.getAllUsers();
        List<ChatContact> chatContactList = new ArrayList<>();
        for (User user : userList
                ) {

            chatContactList.add(new ChatContact(user.getId(), user.getName(), user.getAvatar(), 0));
            Log.d("MainActivity", "user from DB :---------> " + user);
        }
        return chatContactList;
    }
}
