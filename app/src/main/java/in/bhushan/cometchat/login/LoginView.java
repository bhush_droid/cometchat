package in.bhushan.cometchat.login;

import org.json.JSONObject;

/**
 * Created by Bhushan Chaudhari
 */
public interface LoginView {


    void showProgress();

    void removeProgress();
    boolean validateUsernameError();
    void loginSuccess(JSONObject jObj);
    void loginFailed(JSONObject jObj);

}
