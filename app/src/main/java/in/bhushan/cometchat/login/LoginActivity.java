package in.bhushan.cometchat.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONObject;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.common.CommonHelper;
import in.bhushan.cometchat.tabbed.TabbedActivity;


/**
 * Created by Bhushan Chaudhari
 */

public class LoginActivity extends AppCompatActivity implements LoginView {

    private LoginPresenter loginPresenter;
    private ImageView loginBtn;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenter = new LoginPresenterImpl(this);
        init();


    }


    private void init() {
        loginBtn = findViewById(R.id.login);
        editText = findViewById(R.id.uid);

        //listener
        loginBtn.setOnClickListener((View v)->{


            if(validateUsernameError()){

                loginPresenter.login(editText.getText().toString(),this);
                //loginPresenter.CreateUser(editText.getText().toString(),this);

            }
        });
    }

    @Override
    public void showProgress() {
        CommonHelper.showLoadingProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        CommonHelper.removeLoadingProgressDialog();
    }

    @Override
    public boolean validateUsernameError() {

        if(editText.getText().equals("") || editText.getText().equals(null)){
            removeProgress();
            return false;
        }
        return true;
    }

    @Override
    public void loginSuccess(JSONObject jObj) {
        removeProgress();
        Log.d("LoginActivity", "login success -------> " + jObj);
        Intent in = new Intent(this, TabbedActivity.class);
        startActivity(in);
        finish();
    }

    @Override
    public void loginFailed(JSONObject jObj) {

        removeProgress();
        Log.d("LoginActivity", "login Failed -------> " + jObj);
    }

}
