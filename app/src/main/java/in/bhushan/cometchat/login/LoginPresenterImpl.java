package in.bhushan.cometchat.login;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import com.inscripts.interfaces.Callbacks;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonChatPresenterImpl;
import in.bhushan.cometchat.service.ChatService;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.common.PreferenceHelper;

/**
 * Created by Bhushan Chaudhari
 */
public class LoginPresenterImpl implements LoginPresenter {

    private LoginView loginView;
    private CometChat cometChat;
    private CommonChatPresenter commonChatPresenter;

    private Context mContext;

    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        this.cometChat = App.getCometChatInstance();
        this.mContext =  App.getContext();
        this.commonChatPresenter = new CommonChatPresenterImpl();
    }

    @Override
    public void login(String UID,Context mContext) {

        cometChat.loginWithUID(mContext,UID, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                PreferenceHelper.setUID(mContext,UID);
                PreferenceHelper.setLoginStatus(mContext,true);
                //commonChatPresenter.startService();
                commonChatPresenter.getOnlineList();
                commonChatPresenter.getGroupList();
                loginView.loginSuccess(jsonObject);
            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                loginView.loginFailed(jsonObject);
            }
        });

    }
}
