package in.bhushan.cometchat.login;

import android.content.Context;

/**
 * Created by Bhushan Chaudhari
 */
public interface LoginPresenter {
    void login(String UID, Context mContext);
}
