package in.bhushan.cometchat.app;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.inscripts.interfaces.Callbacks;
import com.inscripts.transports.CometserviceChatroom;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import cometchat.inscripts.com.cometchatcore.coresdk.CometChatroom;
import in.bhushan.cometchat.db.AppDatabase;

/**
 * Created by Bhushan Chaudhari
 */
public class App extends MultiDexApplication {


    private static final String  licenseKey = "COMETCHAT-V528X-727B9-XUDUT-WE44H";
    private static final String apiKey = "50025xbe4a6bf14d71cac411d09632401df1de";

    private static Context mContext;
    private static AppDatabase mDB;
    private static CometChat cometChat;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mDB = AppDatabase.getDatabase(mContext);
        //Initialize Comet Chat
        cometChat = CometChat.getInstance(mContext);



        initializeCometChat();
    }

    public static Context getContext() {
        if (mContext == null) {
            mContext = new App().getApplicationContext();
        }
        return mContext;
    }

    public static CometChat getCometChatInstance(){
        if(cometChat ==null){
            cometChat = CometChat.getInstance(getContext());
            initializeCometChat();
        }
        return cometChat;
    }

    public static synchronized AppDatabase getDB() {
        if (mDB == null) {
            mDB = AppDatabase.getDatabase(mContext);
        }
        return mDB;
    }


    private static void initializeCometChat(){
        cometChat.initializeCometChat("", licenseKey, apiKey, true, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {

                Log.d("App", "success --------> " + jsonObject);
            }
            @Override
            public void failCallback(JSONObject jsonObject) {

                Log.d("App", "failed --------> " + jsonObject);


            }
        });

    }
}
