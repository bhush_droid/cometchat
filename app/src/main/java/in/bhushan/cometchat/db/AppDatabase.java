package in.bhushan.cometchat.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import in.bhushan.cometchat.db.dao.GroupDao;
import in.bhushan.cometchat.db.dao.GroupMessageDao;
import in.bhushan.cometchat.db.dao.MessageDao;
import in.bhushan.cometchat.db.dao.ProfileDao;
import in.bhushan.cometchat.db.dao.UserDao;
import in.bhushan.cometchat.pojo.GroupMessage;
import in.bhushan.cometchat.pojo.Groups;
import in.bhushan.cometchat.pojo.Message;
import in.bhushan.cometchat.pojo.Profile;
import in.bhushan.cometchat.pojo.User;

/**
 * Created by Bhushan Chaudhari
 */

@Database(entities = {User.class, Message.class, Profile.class, Groups.class, GroupMessage.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;
    private static final String DB_NAME = "comet_chat";

    public abstract UserDao userModel();
    public abstract MessageDao messageModel();
    public abstract ProfileDao profileModel();
    public abstract GroupDao groupModel();
    public abstract GroupMessageDao groupMessageModel();


    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DB_NAME)
                    .allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}

