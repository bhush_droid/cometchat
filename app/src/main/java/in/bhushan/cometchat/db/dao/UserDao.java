package in.bhushan.cometchat.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import in.bhushan.cometchat.pojo.User;

/**
 * Created by Bhushan Chaudhari
 */

@Dao
public interface UserDao extends BaseDao<User>{


    @Query("SELECT * FROM User")
    List<User> getAllUsers();

    @Query("SELECT NAME FROM USER WHERE ID =:id")
    String[] getUserNameById(String id);

}
