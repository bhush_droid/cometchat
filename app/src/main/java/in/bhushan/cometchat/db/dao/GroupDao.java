package in.bhushan.cometchat.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import in.bhushan.cometchat.pojo.Groups;

/**
 * Created by Bhushan Chaudhari
 */

@Dao
public interface GroupDao extends BaseDao<Groups> {

    @Query("SELECT * FROM GROUPS")
    LiveData<List<Groups>> getAllGroups();

}
