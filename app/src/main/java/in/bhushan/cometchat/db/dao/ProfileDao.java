package in.bhushan.cometchat.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import in.bhushan.cometchat.pojo.Profile;

/**
 * Created by Bhushan Chaudhari
 */

@Dao
public interface ProfileDao extends BaseDao<Profile> {

    @Query("SELECT * FROM PROFILE LIMIT 1")
    Profile[] getProfile();


    @Query("SELECT ID FROM PROFILE LIMIT 1")
    String[] getProfileID();




}
