package in.bhushan.cometchat.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;

import in.bhushan.cometchat.pojo.GroupMessage;
import in.bhushan.cometchat.pojo.Message;

/**
 * Created by Bhushan Chaudhari
 */
@Dao
public interface GroupMessageDao extends BaseDao<GroupMessage> {
    @Query("SELECT * FROM GROUPMESSAGE")
    List<GroupMessage> getAllGroupMessage();

    @Query("SELECT * FROM GROUPMESSAGE WHERE GROUPID = :group_id ORDER BY SENT DESC")
    LiveData<List<GroupMessage>> getGroupMessages(String group_id);

    @Query("SELECT ID FROM GROUPMESSAGE WHERE GROUPID = :group_id ORDER BY SENT DESC LIMIT 1")
    String[] getLatestGroupMessage(String group_id);

}
