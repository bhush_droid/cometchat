package in.bhushan.cometchat.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.bhushan.cometchat.pojo.Message;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;

/**
 * Created by Bhushan Chaudhari
 */

@Dao
public interface MessageDao extends BaseDao<Message> {
    
    @Query("SELECT * FROM MESSAGE")
    List<Message> getAllMessage();

    @Query("SELECT * FROM MESSAGE WHERE MSGFROM = :msgFrom  OR (MSGFROM =(SELECT ID FROM PROFILE LIMIT 1) AND MSGTO =:msgFrom) ORDER BY SENT DESC")
    LiveData<List<Message>> getMessages(String msgFrom);

    @Query("SELECT * FROM MESSAGE WHERE MSGFROM = :msgFrom ORDER BY SENT DESC LIMIT 1")
    List<Message> getLatestMessage(String msgFrom);

}
