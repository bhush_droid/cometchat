package in.bhushan.cometchat.chat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.inscripts.jsonphp.Common;

import java.util.List;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.adapter.ChatAdapter;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonHelper;
import in.bhushan.cometchat.common.Constant;
import in.bhushan.cometchat.pojo.Message;

/**
 * Created by Bhushan Chaudhari
 */
public class ChatActivity extends AppCompatActivity implements ChatView {

    private String uid;
    private EditText editText;
    private ImageView imageView;
    private RecyclerView chatRecyclerView;
    private ChatAdapter chatAdapter;
    private ChatPresenter chatPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        chatPresenter = new ChatPresenterImpl(this);
        uid = getIntent().getStringExtra("id");
        Log.d("ChatActivity", "--------------<>" + uid);

        init();
        chatPresenter.getMessages(uid);


        chatPresenter.getMessagesFromDB(uid).observe(this, (List<Message> msgList) -> {
            Log.d("ChatActivity", "In Observer");
            init_rv(msgList);
        });


    }

    @Override
    public void init() {

        editText = findViewById(R.id.send_msg_et);
        imageView = findViewById(R.id.send_msg_btn);
        chatRecyclerView = findViewById(R.id.chat_rv);

        imageView.setOnClickListener((View v) -> {
            Log.d("ChatActivity", "Image Button Clicked");


            if (editText.getText().length() > 0) {
                showProgress();
                sendMessage();
            } else {
                Toast.makeText(App.getContext(), "Blank Message can't  be sent", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void showProgress() {
        CommonHelper.showLoadingProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        CommonHelper.removeLoadingProgressDialog();
    }

    @Override
    public void init_rv(List<Message> msgList) {
        Log.d("ChatActivity", "init recyclerview");
        chatAdapter = new ChatAdapter(msgList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        chatRecyclerView.setLayoutManager(linearLayoutManager);
        chatRecyclerView.setAdapter(chatAdapter);
    }

    @Override
    public void profileNotSync() {
        removeProgress();
        CommonHelper.showAlertDialog(this,"Warning","Profile Not yet Synced", Constant.ALERT_WARNING);
    }

    @Override
    public void sendMessage() {

        Message message = new Message();
        message.setMsgTo(uid);
        message.setMessage(editText.getText().toString());
        message.setSelf(1);
        message.setMessageType("10");
        chatPresenter.sendMessage(message, false);
    }

    @Override
    public void successMessageSent() {
        CommonHelper.hideKeyboard(this);
        editText.setText("");
        chatPresenter.getMessages(uid);
        Log.d("ChatActivity", "Message sent successfully");
        removeProgress();
    }

}
