package in.bhushan.cometchat.chat;

import android.arch.lifecycle.LiveData;

import java.util.List;

import in.bhushan.cometchat.listener.InternalCallback;
import in.bhushan.cometchat.pojo.Message;

/**
 * Created by Bhushan Chaudhari
 */
public interface ChatPresenter {


    //List<Message> getMessagesFromDB(String id);
    LiveData<List<Message>> getMessagesFromDB(String id);
    void getMessages(String id);
    void sendMessage(Message msg, boolean isGroup);
}
