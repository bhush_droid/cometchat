package in.bhushan.cometchat.chat;

import java.util.List;

import in.bhushan.cometchat.pojo.Message;

/**
 * Created by Bhushan Chaudhari
 */
public interface ChatView {

    void sendMessage();
    void successMessageSent();
    void init();
    void showProgress();
    void removeProgress();
    void init_rv(List<Message> msgList);

    void profileNotSync();
}
