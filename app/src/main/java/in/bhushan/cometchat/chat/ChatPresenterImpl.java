package in.bhushan.cometchat.chat;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.inscripts.interfaces.Callbacks;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonChatPresenterImpl;
import in.bhushan.cometchat.common.PreferenceHelper;
import in.bhushan.cometchat.db.AppDatabase;
import in.bhushan.cometchat.listener.InternalCallback;
import in.bhushan.cometchat.pojo.Message;

/**
 * Created by Bhushan Chaudhari
 */
public class ChatPresenterImpl implements ChatPresenter {

    private LiveData<List<Message>> liveDataMsgList;
    private ChatView chatView;
    private AppDatabase mDb;
    private CometChat cometChat;
    private Context mContext;
    private CommonChatPresenter commonChatPresenter;
    private String current_id;
    public ChatPresenterImpl(ChatView chatView) {
        this.chatView = chatView;
        mDb = App.getDB();
        cometChat = App.getCometChatInstance();
        mContext = App.getContext();
        commonChatPresenter = new CommonChatPresenterImpl();
        getProfileID();
        Log.d("ChatPresenterImpl", "<---" + current_id + "--->");
    }


    private void getProfileID(){
        String[] x = mDb.profileModel().getProfileID();
        if(x.length > 0){
            current_id = x[0];
            Log.d("ChatPresenterImpl", "<---**** " + current_id + " ****--->");
        }else{
            commonChatPresenter.restartService();
            current_id = null;
        }

    }

    @Override
    public LiveData<List<Message>> getMessagesFromDB(String id) {
        //return mDb.messageModel().getAllMessage();
         return mDb.messageModel().getMessages(id);
    }

    @Override
    public void getMessages(String id) {
        //mDb.messageModel().getMessages(id);
        getChatHistory(Long.parseLong(id),0);
        //List<Message> msgList = mDb.messageModel().getLatestMessage(id);
        /*Log.d("ChatPresenterImpl", "---------Latest Message-------->" + msgList);
        if(msgList.size() > 0){
            getChatHistory(Long.parseLong(id),Long.parseLong(msgList.get(0).getId()));
        }else{
            //get All chat History
            getChatHistory(Long.parseLong(id),0);
        }*/
    }


    private void getChatHistory(long id, long lastMsgId){
        cometChat.getChatHistory(id, lastMsgId, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("ChatPresenterImpl", "Messages------------->" + jsonObject);
                commonChatPresenter.setMessages(jsonObject);
            }

            @Override
            public void failCallback(JSONObject jsonObject) {

            }
        });

    }

    @Override
    public void sendMessage(Message msg,boolean isGroup) {
        getProfileID();
        if(current_id==null){
            commonChatPresenter.restartService();
            chatView.profileNotSync();
            return;
        }

        msg.setMsgFrom(current_id);
        Log.d("ChatPresenterImpl", "<------- InSendMessage -------->");
        cometChat.sendMessage(Long.parseLong(current_id), msg.getMsgTo(), msg.getMessage(), isGroup, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("ChatPresenterImpl", "<------- Send Message -------->  " + jsonObject);
                try {
                    msg.setSent(Long.parseLong(jsonObject.getString("sent")));
                    msg.setId(String.valueOf(jsonObject.get("id")));
                    mDb.messageModel().insert(msg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                chatView.successMessageSent();
            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.d("ChatPresenterImpl", "<------- Send Message failed -------->  " + jsonObject);

                if(jsonObject.has("error")){
                    try {
                        Toast.makeText(mContext, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                chatView.removeProgress();
            }
        });

    }


}
