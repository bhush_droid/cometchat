package in.bhushan.cometchat.GroupChat;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.inscripts.interfaces.Callbacks;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonChatPresenterImpl;
import in.bhushan.cometchat.db.AppDatabase;
import in.bhushan.cometchat.pojo.GroupMessage;
import in.bhushan.cometchat.pojo.Message;
import in.bhushan.cometchat.pojo.Profile;

/**
 * Created by Bhushan Chaudhari
 */
public class GroupChatPresenterImpl implements GroupChatPresenter {

    private GroupChatView groupChatView;
    private AppDatabase mDb;
    private CometChat cometChat;
    private Context mContext;
    private CommonChatPresenter commonChatPresenter;
    private String current_id;

    public GroupChatPresenterImpl(GroupChatView groupChatView) {
        this.groupChatView = groupChatView;
        mDb = App.getDB();
        cometChat = App.getCometChatInstance();
        mContext = App.getContext();
        init_current();
        Log.d("ChatPresenterImpl", "<---" + current_id + "--->");
        commonChatPresenter = new CommonChatPresenterImpl();

    }


    private void init_current() {
        String[] x = mDb.profileModel().getProfileID();
        if (x.length > 0) {
            current_id = x[0];
        } else {
            commonChatPresenter.getProfileInfo();
        }

    }

    private void getGroupChatHistory(long id, long lastMsgId) {
        cometChat.getGroupChatHistory(id, lastMsgId, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("GroupChatPresenterImpl", "<--Group Chat History-->" + jsonObject);
                commonChatPresenter.setGroupMessages(jsonObject);
            }

            @Override
            public void failCallback(JSONObject jsonObject) {

            }
        });
    }


    @Override
    public LiveData<List<GroupMessage>> getMessagesFromDB(String id) {
        return mDb.groupMessageModel().getGroupMessages(id);
    }

    @Override
    public void getMessages(String id) {
        getGroupChatHistory(Long.parseLong(id), 0);
    }

    @Override
    public void sendMessage(GroupMessage msg, boolean isGroup) {
        init_current();
        if(current_id==null){
            commonChatPresenter.restartService();
            groupChatView.profileNotSync();
        }





        msg.setFrom(current_id);
        Log.d("GroupChatPresenterImpl", "<------- InSendMessage -------->");
        cometChat.sendMessage(Long.parseLong(current_id), msg.getGroupid(), msg.getMessage(), isGroup, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("GroupChatPresenterImpl", "<------- Send Message -------->  " + jsonObject);
                try {
                    msg.setSent(jsonObject.getString("sent"));
                    msg.setId(String.valueOf(jsonObject.get("id")));
                    msg.setFromid(current_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mDb.groupMessageModel().insert(msg);

                groupChatView.successMessageSent();

            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.d("ChatPresenterImpl", "<------- Send Message failed -------->  " + jsonObject);
                if (jsonObject.has("error")) {
                    try {
                        Toast.makeText(mContext, jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


                groupChatView.removeProgress();
            }
        });
    }

    @Override
    public String getProfileID() {
        return mDb.profileModel().getProfileID()[0];
    }
}
