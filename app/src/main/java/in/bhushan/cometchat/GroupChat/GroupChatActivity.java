package in.bhushan.cometchat.GroupChat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.adapter.GroupChatAdapter;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.common.CommonHelper;
import in.bhushan.cometchat.common.Constant;
import in.bhushan.cometchat.pojo.GroupMessage;

/**
 * Created by Bhushan Chaudhari
 */

public class GroupChatActivity extends AppCompatActivity implements GroupChatView {


    private String uid;
    private EditText editText;
    private ImageView imageView;
    private RecyclerView chatRecyclerView;
    private GroupChatAdapter groupChatAdapter;
    private GroupChatPresenter groupChatPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);

        uid = getIntent().getStringExtra("id");
        groupChatPresenter = new GroupChatPresenterImpl(this);
        init();
        groupChatPresenter.getMessages(uid);

        groupChatPresenter.getMessagesFromDB(uid).observe(this,(List<GroupMessage> groupMsg)->{

            init_rv(groupMsg);
        });

    }

    @Override
    public void init() {
        editText = findViewById(R.id.send_msg_et);
        imageView = findViewById(R.id.send_msg_btn);
        chatRecyclerView = findViewById(R.id.chat_rv);

        imageView.setOnClickListener((View v) -> {
            Log.d("ChatActivity", "Image Button Clicked");
            if (editText.getText().length() > 0) {
                showProgress();
                sendMessage();
            } else {
                Toast.makeText(App.getContext(), "Blank Message can't  be sent", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void showProgress() {
        CommonHelper.showLoadingProgressDialog(this);
    }

    @Override
    public void removeProgress() {
        CommonHelper.removeLoadingProgressDialog();
    }

    @Override
    public void init_rv(List<GroupMessage> msgList) {
        Log.d("ChatActivity", "init recyclerview");
        groupChatAdapter = new GroupChatAdapter(msgList,groupChatPresenter.getProfileID());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        chatRecyclerView.setLayoutManager(linearLayoutManager);
        chatRecyclerView.setAdapter(groupChatAdapter);
    }

    @Override
    public void profileNotSync() {
        removeProgress();
        CommonHelper.showAlertDialog(this,"Warning","Profile Not yet Synced", Constant.ALERT_WARNING);
    }


    @Override
    public void sendMessage() {GroupMessage message = new GroupMessage();

        message.setGroupid(uid);
        message.setMessage(editText.getText().toString());
        message.setMessageType(10);

        groupChatPresenter.sendMessage(message, true);
    }

    @Override
    public void successMessageSent() {
        CommonHelper.hideKeyboard(this);
        editText.setText("");
        Log.d("ChatActivity", "Message sent successfully");
        removeProgress();
    }
}
