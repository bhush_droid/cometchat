package in.bhushan.cometchat.GroupChat;

import android.arch.lifecycle.LiveData;

import java.util.List;

import in.bhushan.cometchat.pojo.GroupMessage;
import in.bhushan.cometchat.pojo.Message;
import in.bhushan.cometchat.pojo.Profile;

/**
 * Created by Bhushan Chaudhari
 */
public interface GroupChatPresenter {


    LiveData<List<GroupMessage>> getMessagesFromDB(String id);
    void getMessages(String id);
    void sendMessage(GroupMessage msg, boolean isGroup);
    String getProfileID();
}
