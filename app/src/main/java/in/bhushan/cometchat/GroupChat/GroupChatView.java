package in.bhushan.cometchat.GroupChat;

import java.util.List;

import in.bhushan.cometchat.pojo.GroupMessage;
import in.bhushan.cometchat.pojo.Message;

/**
 * Created by Bhushan Chaudhari
 */
public interface GroupChatView {

    void sendMessage();
    void successMessageSent();
    void init();
    void showProgress();
    void removeProgress();
    void init_rv(List<GroupMessage> msgList);

    void profileNotSync();
}
