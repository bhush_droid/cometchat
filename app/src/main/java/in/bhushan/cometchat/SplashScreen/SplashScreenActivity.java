package in.bhushan.cometchat.SplashScreen;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import in.bhushan.cometchat.R;
import in.bhushan.cometchat.login.LoginActivity;
import in.bhushan.cometchat.tabbed.TabbedActivity;


/**
 * Created by Bhushan Chaudhari
 */
public class SplashScreenActivity extends AppCompatActivity implements SplashScreenView {


    SplashScreenPresenter splashScreenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        splashScreenPresenter = new SplashScreenPresenterImpl(this);
        requestRealTimePermissions();
    }

    @Override
    public void checkLoginStatus() {

        boolean loginStatus = splashScreenPresenter.checkLoginStatus();

        Handler h = new Handler();
        h.postDelayed(() -> {
            if (loginStatus) {
                //startActivity(new Intent(this, MainActivity.class));
                startActivity(new Intent(this, TabbedActivity.class));
                finish();
            } else {
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            }

        }, 3 * 1000);

    }

    @Override
    public void requestRealTimePermissions() {


        int permsRequestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] perms = {"android.permission.READ_PHONE_STATE",
                    "android.permission.WRITE_EXTERNAL_STORAGE"};
            requestPermissions(perms, permsRequestCode);
        } else {
            checkLoginStatus();
        }


    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:
                boolean phoneStateAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean writeStorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                checkLoginStatus();
                break;

        }

    }

    @Override
    public void initiateCometChat() {

    }
}
