package in.bhushan.cometchat.SplashScreen;

/**
 * Created by Bhushan Chaudhari
 */
public interface SplashScreenPresenter {

    boolean checkLoginStatus();

}
