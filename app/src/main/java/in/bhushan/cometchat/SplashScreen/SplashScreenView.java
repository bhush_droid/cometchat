package in.bhushan.cometchat.SplashScreen;

/**
 * Created by Bhushan Chaudhari
 */
public interface SplashScreenView {

    void checkLoginStatus();
    void requestRealTimePermissions();
    void initiateCometChat();
}
