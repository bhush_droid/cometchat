package in.bhushan.cometchat.SplashScreen;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.inscripts.interfaces.Callbacks;

import org.json.JSONObject;

import cometchat.inscripts.com.cometchatcore.coresdk.CometChat;
import in.bhushan.cometchat.common.CommonChatPresenter;
import in.bhushan.cometchat.common.CommonChatPresenterImpl;
import in.bhushan.cometchat.service.ChatService;
import in.bhushan.cometchat.app.App;
import in.bhushan.cometchat.common.PreferenceHelper;

/**
 * Created by Bhushan Chaudhari
 */
public class SplashScreenPresenterImpl implements SplashScreenPresenter{

    private SplashScreenView splashScreenView;
    private CometChat cometChat;
    private Context mContext;
private CommonChatPresenter commonChatPresenter;

    public SplashScreenPresenterImpl(SplashScreenView splashScreenView) {
        this.splashScreenView = splashScreenView;
        cometChat = App.getCometChatInstance();
        this.mContext = App.getContext();
        commonChatPresenter = new CommonChatPresenterImpl();
    }

    @Override
    public boolean checkLoginStatus() {
        boolean status =  PreferenceHelper.getLoginStatus(mContext);
        if(status) {
            //login();

            //commonChatPresenter.startService();
            commonChatPresenter.getOnlineList();
            commonChatPresenter.getGroupList();
        }
        return status;
    }

    public void login(){
        cometChat.loginWithUID(mContext,PreferenceHelper.getUID(mContext), new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("SplashScreenPresenterIm", "json --->" + jsonObject);

                //commonChatPresenter.startService();
            }
            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.d("SplashScreenPresenterIm", "failed --->" + jsonObject);
            }
        });
    }

}
