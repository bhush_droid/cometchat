package in.bhushan.cometchat.pojo;

/**
 * Created by Bhushan Chaudhari
 */

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@Entity
public class User {

    @PrimaryKey
    @SerializedName("id")
    @NonNull
    private String id;

    @SerializedName("n")
    private String name;

    @SerializedName("a")
    private String avatar;

    @SerializedName("d")
    private String userGroup;

    @SerializedName("s")
    private String status;

    @SerializedName("m")
    private String statusMessage;

    @SerializedName("l")
    private String link;

    @SerializedName("ls")
    private String lastSeen;

    @SerializedName("lsn")
    private String lastSeenStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getLastSeenStatus() {
        return lastSeenStatus;
    }

    public void setLastSeenStatus(String lastSeenStatus) {
        this.lastSeenStatus = lastSeenStatus;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", userGroup='" + userGroup + '\'' +
                ", status='" + status + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", link='" + link + '\'' +
                ", lastSeen='" + lastSeen + '\'' +
                ", lastSeenStatus='" + lastSeenStatus + '\'' +
                '}';
    }
}