package in.bhushan.cometchat.pojo;

/**
 * Created by Bhushan Chaudhari
 */

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Profile {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    @NonNull
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("l")
    @Expose
    private String l;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("ch")
    @Expose
    private String ch;
    @SerializedName("lastSeen")
    @Expose
    private String lastSeen;
    @SerializedName("lastSeenStatus")
    @Expose
    private String lastSeenStatus;
    @SerializedName("rdrs")
    @Expose
    private String rdrs;
    @SerializedName("push_channel")
    @Expose
    private String pushChannel;
    @SerializedName("ccmobileauth")
    @Expose
    private long ccmobileauth;
    @SerializedName("push_an_channel")
    @Expose
    private String pushAnChannel;
    @SerializedName("webrtc_prefix")
    @Expose
    private String webrtcPrefix;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getCh() {
        return ch;
    }

    public void setCh(String ch) {
        this.ch = ch;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getLastSeenStatus() {
        return lastSeenStatus;
    }

    public void setLastSeenStatus(String lastSeenStatus) {
        this.lastSeenStatus = lastSeenStatus;
    }

    public String getRdrs() {
        return rdrs;
    }

    public void setRdrs(String rdrs) {
        this.rdrs = rdrs;
    }

    public String getPushChannel() {
        return pushChannel;
    }

    public void setPushChannel(String pushChannel) {
        this.pushChannel = pushChannel;
    }

    public long getCcmobileauth() {
        return ccmobileauth;
    }

    public void setCcmobileauth(long ccmobileauth) {
        this.ccmobileauth = ccmobileauth;
    }

    public String getPushAnChannel() {
        return pushAnChannel;
    }

    public void setPushAnChannel(String pushAnChannel) {
        this.pushAnChannel = pushAnChannel;
    }

    public String getWebrtcPrefix() {
        return webrtcPrefix;
    }

    public void setWebrtcPrefix(String webrtcPrefix) {
        this.webrtcPrefix = webrtcPrefix;
    }


    @Override
    public String toString() {
        return "Profile{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", l='" + l + '\'' +
                ", avatar='" + avatar + '\'' +
                ", status='" + status + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", ch='" + ch + '\'' +
                ", lastSeen='" + lastSeen + '\'' +
                ", lastSeenStatus='" + lastSeenStatus + '\'' +
                ", rdrs='" + rdrs + '\'' +
                ", pushChannel='" + pushChannel + '\'' +
                ", ccmobileauth=" + ccmobileauth +
                ", pushAnChannel='" + pushAnChannel + '\'' +
                ", webrtcPrefix='" + webrtcPrefix + '\'' +
                '}';
    }
}
