package in.bhushan.cometchat.pojo;

/**
 * Created by Bhushan Chaudhari
 */

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



@Entity
public class Message {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    @NonNull
    private String id;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("self")
    @Expose
    private int self;
    @SerializedName("old")
    @Expose
    private String old;
    @SerializedName("sent")
    @Expose
    private long sent;
    @SerializedName("from")
    @Expose
    private String msgFrom;
    @SerializedName("message_type")
    @Expose
    private String messageType;

    @SerializedName("to")
    @Expose
    private String msgTo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSelf() {
        return self;
    }

    public void setSelf(int self) {
        this.self = self;
    }

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }

    public long getSent() {
        return sent;
    }

    public void setSent(long sent) {
        this.sent = sent;
    }

    public String getMsgFrom() {
        return msgFrom;
    }

    public void setMsgFrom(String from) {
        this.msgFrom = from;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMsgTo() {
        return msgTo;
    }

    public void setMsgTo(String msgTo) {
        this.msgTo = msgTo;
    }


    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", message='" + message + '\'' +
                ", self=" + self +
                ", old='" + old + '\'' +
                ", sent=" + sent +
                ", msgFrom='" + msgFrom + '\'' +
                ", messageType='" + messageType + '\'' +
                ", msgTo='" + msgTo + '\'' +
                '}';
    }
}