package in.bhushan.cometchat.pojo;

/**
 * Created by Bhushan Chaudhari
 */

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Groups {

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("noOfMembers")
    @Expose
    private String noOfMembers;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("pass")
    @Expose
    private String pass;
    @SerializedName("userStatus")
    @Expose
    private long userStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoOfMembers() {
        return noOfMembers;
    }

    public void setNoOfMembers(String noOfMembers) {
        this.noOfMembers = noOfMembers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public long getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(long userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public String toString() {
        return "Groups{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", noOfMembers='" + noOfMembers + '\'' +
                ", type='" + type + '\'' +
                ", pass='" + pass + '\'' +
                ", userStatus=" + userStatus +
                '}';
    }
}
