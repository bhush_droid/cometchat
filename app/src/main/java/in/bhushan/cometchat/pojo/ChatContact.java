package in.bhushan.cometchat.pojo;

import android.support.annotation.NonNull;

/**
 * Created by Bhushan Chaudhari
 */
public class ChatContact{

    private String id;
    private String name;
    private String avatar;
    private int isGroup;
private int lastMsg;


    public ChatContact(String id, String name, String avatar, int isGroup) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.isGroup = isGroup;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(int isGroup) {
        this.isGroup = isGroup;
    }


    public int getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(int lastMsg) {
        this.lastMsg = lastMsg;
    }

    @Override
    public String toString() {
        return "ChatContact{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", isGroup=" + isGroup +
                ", lastMsg=" + lastMsg +
                '}';
    }
}
