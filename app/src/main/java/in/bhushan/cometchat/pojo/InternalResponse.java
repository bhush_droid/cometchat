package in.bhushan.cometchat.pojo;

import com.google.gson.JsonObject;

import org.json.JSONObject;

/**
 * Created by Bhushan Chaudhari on 7/4/2018 12:39 PM.
 */
public class InternalResponse {

    private JSONObject response;
    private boolean status;


    public InternalResponse(JSONObject response, boolean status) {
        this.response = response;
        this.status = status;
    }

    public JSONObject getResponse() {
        return response;
    }

    public void setResponse(JSONObject response) {
        this.response = response;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
